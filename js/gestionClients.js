var clientsObtenidos;

function gestionCustomers() {
  var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Customers"
  var request = new XMLHttpRequest();

  request.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        // console.log(request.responseText);
        console.table(JSON.parse(request.responseText).value);

        clientsObtenidos = request.responseText;
        procesarCustomers()
      }
  }

  request.open("GET", url, true);
  request.send();
}


function  procesarCustomers() {

  var JSONClients = JSON.parse(clientsObtenidos)

  var divTable = document.getElementById("divTablaCustomers"); // divTablaCustomers;
  // var divTable = divTablaCustomers;

  var tabla = document.createElement("table");
  var tbody = document.createElement("tbody");

  tabla.classList.add("table")
  tabla.classList.add("table-striped")


  // https://www.countries-ofthe-world.com/flags-normal/flag-of-Afghanistan.png
  var urlBandera = "https://www.countries-ofthe-world.com/flags-normal/flag-of-"

  for(let i=0; i<JSONClients.value.length; i++) {
       console.log("Clients Name [" + i + "] " + JSONClients.value[i].ProductName);

      var nuevaFila = document.createElement("tr");

      var columnaNombre = document.createElement("td");
      columnaNombre.innerText = JSONClients.value[i].ContactName;

      var columnaAddress = document.createElement("td");
      columnaAddress.innerText = JSONClients.value[i].Address;

      var columnaPais  = document.createElement("td");
      // columnaPais.innerText = JSONClients.value[i].Country;

      var imgBandera = document.createElement("img");

      if (JSONClients.value[i].Country == "UK") {
          imgBandera.src = urlBandera + "United-Kingdom.png"
      } else {
          imgBandera.src = urlBandera + JSONClients.value[i].Country + ".png"
      }


      imgBandera.classList.add("flag");

      columnaPais.appendChild(imgBandera);


      nuevaFila.appendChild(columnaNombre);
      nuevaFila.appendChild(columnaAddress);
      nuevaFila.appendChild(columnaPais);

      tbody.appendChild(nuevaFila)

  }

  tabla.appendChild(tbody)

  divTable.appendChild(tabla)

}
